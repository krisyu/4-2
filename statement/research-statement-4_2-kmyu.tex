%Time-stamp: <2015-02-12 09:40:57 amoebe>
\documentclass[12pt]{article}

\usepackage{caption,subcaption,pdflscape}
\usepackage{latexsym,amsmath,amssymb,amsthm}
\usepackage{graphicx,epic,eepic,color,hyperref} 
\usepackage{epsexe}
\usepackage{timestamp}
\usepackage[letterpaper,top=2.5cm,bottom=2cm,left=2cm,right=2cm,head=1cm,foot=1cm,marginpar=30pt]{geometry}
\usepackage[x11names, rgb]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{lmodern}
\usepackage{times}
\usepackage[american]{babel}

\usepackage{natbib}

\usepackage[tone]{tipa}
\let\ipa\textipa

\definecolor{neonblue}{rgb}{0.3,0.3,1}
\definecolor{irishflag}{rgb}{0.0,0.6,0.0}
\definecolor{firebrick}{rgb}{0.56,0.14,0.14}
\definecolor{navy}{rgb}{0.0,0.0,0.5}

\hypersetup{colorlinks,breaklinks,
            linkcolor=firebrick,urlcolor=neonblue,
             anchorcolor=navy,citecolor=irishflag}
\usepackage{fancyhdr}
\usepackage{lastpage}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\newenvironment{packed_enum}{
\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{enumerate}}

\newenvironment{packed_item}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{itemize}}

%\setlength{\bibsep}{0.0pt}
\setlength{\headheight}{15.2pt}
\setlength{\headsep}{12pt}
\pagestyle{fancyplain}
\fancyhf{}
\lhead{\textbf{4.2 research statement}}
\rhead{Kristine M. Yu, UMass Department of Linguistics}

\rfoot{\thepage\ of \pageref{LastPage}}

\title{Research statement}

\begin{document}
\date{}
%\maketitle

%\setcounter{section}{-1}

\section{Introduction}

A universal of spoken language is that every language exploits variation
in pitch in a linguistically meaningful way. In English, uttering \textit{I
drank four bottles of Ginger Libation last night} with a falling pitch
at the end marks the utterance as a statement of fact. However, uttering
the same sentence with a rising pitch at the end marks it as a
question someone might ask a friend upon waking up in the morning with a major hangover. In
Cantonese, uttering \textit{lau} with a falling pitch means ``stay'',
while uttering \textit{lau} with a rising pitch means ``twist''. In
Somali, uttering \textit{kalax} with a high-low tone sequence means
``ladle'', but uttering it with a low-high sequence means ``ladles''. But variation in pitch can also provide socioaffective cues---for instance,
someone might raise their voice if they are irate. A case in point is that mothers speaking to their infants
use a higher and wider pitch range (compared to speaking to another
adult) across a wide variety of languages, and high pitch and pitch
modulations have been shown to help draw and maintain the attention of
infants. This dual function of pitch manipulation---both to cue
linguistic structure and to signal socioaffective cues---is a symptom
of a classic problem that has dogged the study of linguistic tone and
intonation. On the one hand, there's only so many things humans can do with pitch---you
can stay steady, you can go up, or you can go down. Phonological analyses of
tone and intonational systems across the world frequently rely on just
two atomic symbols: H for high, and L for low. On the other hand,
the multitude and diversity of \textit{sources} of tones is
astounding. In addition to socioaffective contexts---as I have hinted
at already, sources from linguistic structures for tones include lexical representations, inflectional
morphology, phonology, syntax, truth-conditional semantics, and pragmatics.\footnote{See the
  introduction in \citet{Yu:2014d} for examples of each mentioned source.}

The focus of my research program is understanding what tones can tell
language comprehenders and learners about grammatical structure---with
an emphasis on real-time language comprehension, and the learnability of
both tonal concepts as well as the relations between tones and morphosyntactic
structures. To ensure that this research program can be successful, I
insist on rooting this work in the details of the speech signal of
real speech data, as I will explain. 

\medskip

How to unravel anything about the relation between pitch and
linguistic structure when tones have so many sources, including
extralinguistic ones? One of my projects, \citet{Yu:2014b,Yu:2015},
tackles this head on in the arena of infant-directed speech (\S\ref{sec:ids}). The rest
of my projects follow Plato's methodological strategy from
\textit{Phaedrus} in carving nature at its joints, setting some
contextual factors aside to home in on what is of primary interest. I
do this by focusing on tonal phenomena of lexical (\S\ref{sec:lexical}) and morphological
contrast (\S\ref{sec:morph}).    

\section{The interaction of socioaffective context and grammar in the
  intonation of infant directed speech}
\label{sec:ids}

I turn first to my project on infant-directed speech. Despite the
classic puzzle of how pitch variability might signal both linguistic
structure and socioaffective cues, I am aware of almost no studies that have
actually investigated how speakers negotiate using pitch variability when
signalling socioaffective cues and linguistic structure may cause
conflicts in their choice of prosodic structure. Such studies are timely as a not unsizeable contingent of prosody
researchers have been moving towards studying prosody as purely
socioaffective signals. Together with my collaborators, we collected
recordings of parents of young infants and children reading a
narrative using read speech\footnote{the typical kind of ``lab speech'' used in
  developing intonational phonological analyses} and
(simulated)\footnote{We did not end up having infants in the room
  during recording because they were too fussy, induced many
  disfluencies and restarts in the parents' speech, and cried loud
  enough to render the recorded speech unanalyzable. Given that almost
  no intonational phonological work has been done on infant-directed
  speech, we decided to abstract away from the effects of
  infant-induced disfluencies at the present time.} infant-directed speech in Bengali and
English. Having the read speech as a baseline allowed us to assess how
the context of infant-directed speech affected parents' prosodic choices. Why Bengali and English? They have very
different prosodic systems, in that Bengali is a language in which the
phonological grammar inserts tones effecting a low-high rise over every accentual
phrases. Flexibility in prosodic choice mostly occurs in the large inventory
of intonational phrase boundary tones, and these are what are
manipulated in focus and topicalization. In contrast, flexibility in
English intonation is highest in the choice of pitch accent, and pitch
accents are an important resource in signalling information
structure. Thus, while parents speaking Bengali and parents speaking
English might all make the same prosodic choices to expand pitch range
and increase pitch modulation to elicit and maintain infant attention,
these prosodic choices might be implemented differently according to
their respective language-specific intonational grammar.  

We found that indeed, Bengali and English speakers expanded pitch
range and increased pitch variability overall in infant-directed
speech. However, the implementation of these gross effects was
constrained by language-specific intonational grammar. First, the range of pitch accents
and edge tones used by parents fell within the range of those licit in the
intonational grammar. Second, both groups showed an increase in
prosodic marking of information structure, but Bengali speakers did so
with phrasing choices, while English speakers did so with pitch accent
choices. Both Bengali and English speakers showed an
increase in grouping speech into larger prosodic constituents
(intonational phrases), but while the distribution over different
boundary tones in English remained constant, the distribution of
boundary tones in Bengali skewed towards those with more inflection
points in pitch (inducing more pitch modulation). Thus, we
demonstrated that intonational choices are jointly constrained by both
socioaffective context and grammatical structure. 


\section{The phonetics of lexical tone contrast}
\label{sec:lexical}

I now turn towards my work on lexical and morphological tone,
beginning with lexical tone. I began working on the phonetics of
lexical tonal contrasts and the learnability of tonal concepts for my dissertation. Although I have interests
in tones from all sources, I chose to focus on lexical tones because
the gross definition of what a tonal concept is here is clear: it's a
map between some set of real-valued phonetic dimensions and a finite set of
abstract tonemes. In contrast, what the basic elements in intonation
are is still poorly understood and controversial. In working on my
dissertation, I initially was going to dive right in to computational
modeling of the acquisition of tone, but quickly realized that this
was a mistake for two reasons: (1) the range of phonetic tonal
production data available from the scientific community to serve as
input to the learner was incredibly limited, and (2) what the relevant
phonetic dimensions of tonal contrast are was still unclear. Formal
learnability theoretic results for category learning rely on the shapes or
properties of the distributions of those categories over some
space, and of course category shapes and distributions depend crucially on
the dimensions of the space. Proof-of-concept learnability results from simulations are
always conditioned on the assumptions made in those simulations. For
instance, if I conclude that tonal categories in Mandarin are not
learnable because a clustering algorithm failed to find four clusters,
this is a false conclusion. I must qualify the conclusion by (among
other things), the assumptions I have made about phonetic
information about tones that have been made available to the learner,
e.g.~that the phonetic input to the learner included only average
fundamental frequency over the syllable (fundamental frequency is the
rate of vocal fold vibration and the acoustic correlate of perceived pitch). Therefore, my dissertation work on learning tones shifted to focus on improving the
tonal production data available, and to carrying out tonal production and
perception studies and analyzing them with statistical/computational
tools (so called ``supervised learning'' algorithms) to better understand the phonetic space of tonal
contrast. 

In addition to conducting my own fieldwork, my reaction to the limited
amount of available phonetic tonal
production data has also been to engage closely with the rest of the
fieldwork community to foster more fieldwork
focusing on aspects of tonal phonetics and phonology. I've made a
point to include an educational component to my talks and publications
about how to uncover underlying tonal contrasts and elicit and work with tonal
phonetic data. For instance, \citet{Yu:2014a} is an extensive
methodological paper I wrote illustrating how to
study tone from the speech signal from the systematic approach of
experimental design, using the Papua New Guinean language of Kirikiri
as an example. It includes substantial supplemental material on
my webpage on practical aspects of recording and analyzing tonal
phonetic data (\href{http://www.krisyu.org/blog/posts/2013/06/ldc-kiy-overview/}{http://www.krisyu.org/blog/posts/2013/06/ldc-kiy-overview/}). \citet{Yu:2014d} likewise includes a detailed
explanation of my methods in designing stimuli to elicit in
intonational fieldwork. 

\citet{Yu:2010,Yu:2011,Yu:2011a,Yu:2014,Yu:2014c} come from research
that was based on my dissertation work. There are two main areas in
I which I have made contributions to understanding the phonetic space of tones here. The
first is in the role of phonation, in particular, creaky voice
quality in tonal contrast. The second is in the nature of the detail
in the shape of pitch contours in tonal representation. I describe my
work on phonation in \S\ref{sec:creaky} and my work on detail in the
shape of pitch contours in \S\ref{sec:res}. I then discuss current
computational work that builds on this prior work in \S\ref{sec:cats}.

\subsection{The role of creaky phonation in tonal contrast}
\label{sec:creaky}

 In
\citet{Yu:2010,Yu:2011,Yu:2014}\footnote{All my work on Cantonese
  was aided by invaluable help from my undergraduate student collaborators.}, I show that even in a language like
Cantonese---where the presence or absence of creak does not by itself result in a
contrast in word meaning---language users nevertheless use creak as a
cue in signaling and recovering tonal contrasts. In this work, I first
established that creak occurred significantly more frequently on the production of
Tone \ipa{\tone{21}} (here, I use a standard iconic transcription
system for tones; the tone indicated here starts quite low in the
pitch range and then falls) than the other five Cantonese tones in a corpus
of data I collected from 8 speakers---prior to my work, the only reports of creak in
Cantonese tone were anecdotal. Then, I performed two experiments to study the role of creak in Cantonese tone
perception. In the first, I checked to see whether the presence of
creak had any effect on accuracy in tonal identification, especially
for Tone \ipa{\tone{21}}. I found that listeners identified this tone
with 20\% more accuracy when it was realized with creak than when it
was not. In the second, I showed that in a two-alternative forced
choice task of identifying stimuli as Tone \ipa{\tone{21}} or Tone
\ipa{\tone{22}} isolating creak from any concomitant pitch cues,
listeners had a higher proportion of Tone 4 responses for creaky
stimuli. In addition, listeners had more Tone 4 responses for creaky
stimuli with longer durations of nonmodal phonation.     

One significant
practical implication of this research is that in work on languages
with tonal contrasts---even languages where phonation is a secondary cue---
in linguistics, speech development and pathology, and automatic speech
recognition (like Siri), researchers need to pay attention to acoustic
dimensions of voice quality beyond just those corresponding to
pitch.\footnote{As an example of the typical state of affairs for
  ``non-exotic'' tone languages where pitch is the primary dimension
  of tonal contrast, consider \citet[p.~29]{Whalen:1992} on recording
  stimuli for a Mandarin tone perception experiment:

\begin{quote}
  \textsf{\scriptsize{Since we knew we would have to exclude any Tone 3
    productions which had creaky voice, we obtained twice as many Tone
    3 syllables as the others.}}
\end{quote}

There are many other such examples of abstracting away from voice
quality beyond fundamental frequency in the linguistics, clinical, and
engineering literature in much more recent work.
} To make doing such research accessible to a wide range of scientists,
my colleagues and I have developed user-friendly software for
automatic extraction of acoustic voice quality parameters
\cite{Shue:2011}. In addition, I have been working with linguistics
and computer science undergraduates and master students at UMass on further
developing this software to be cross-platform and more efficient at
batch processing
(\href{https://github.com/voicesauce}{https://github.com/voicesauce}).  

The basic consequence of this work for studying the learnability of tone is
that parameters capturing voice quality beyond fundamental frequency
must be included in input to the learner. In fact, as I've discovered
(and noted in \citet{Yu:2010}), it is \textit{impossible} to abstract away from
voice quality even if one defines a phonetic space using only
fundamental frequency. This is because fundamental frequency is
ill-defined when the rate of vocal fold vibration is very irregular
and this is what happens in creaky phonation. Pitch tracking
algorithms often fail to return fundamental frequency values during regions of creaky
phonation (and we still don't know what the human pitch tracking
algorithm does). The challenge that my and others' work on phonation
in tonal contrast raises for the learner is how to determine how much
attention to pay to phonetic dimensions of phonation in learning tonal categories.
I'll return to this point in \S\ref{sec:cats}.


\subsection{Temporal resolution in tonal representation}
\label{sec:res}

Another aspect of the phonetic space of tonal contrast that has drawn
my attention is the fineness of detail in the pitch contour (and time
courses of other phonetic parameters) that is relevant for tonal
concepts. Suppose there were a tone with a pitch contour over the syllable like this:
\ipa{\tone{2534124}}. If I sampled just three points over the syllable
as a perceiver,
there is no way I would be able to capture all the inflection points
in the pitch contour---I might end up with the percept of a shape like this:
\ipa{\tone{251}}. Indeed, Chao, who introduced the iconic tone letters
\citep{Chao:1930} used in the International Phonetic Alphabet for
representing linguistic tone, wrote: ``the exact shape of the
time-pitch curve, so far as I have observed, has never been a
necessary distinctive feature, given the starting and ending points,
or the turning point, if any, on the five-point scale''
\citep[p.~25]{Chao:1968}, and tone letters are understood to have up to 3
samples, e.g.\ \ipa{\tone{213}}. This idea that fine temporal
resolution of the speech signal is not necessary for the
parameterization of tone is the consensus view.  But at an international
workshop on the sound of tonal contrast I organized in June 2014, 
an emerging aspect of tonal contrast that my colleagues brought up was
the necessity of fine temporal detail in pitch contours. For instance, \citet{Remijsen:2013} and subsequent work
has found that a difference of a mere 40-64 ms in the onset of an pitch
fall signals a contrast in word meaning in Dinka and Shilluk,
languages of South Sudan. In other words, \citet{Remijsen:2013}'s work
suggests that the consensus view is wrong, and the question of how fine or coarse
temporal resolution must be in defining tonal concepts is an open
one. 

In \citet{Yu:2011a,Yu:2014c}, I take on this question using acoustic
and perceptual studies of Cantonese tone. I find that although
sampling resolution had little effect on tonal identification
accuracy, and that just
two samples of fundamental frequency per syllable are sufficient for
listeners to identify tones at well above chance levels. However, a
closer look at the results for the highly confusable rises, Tone
\ipa{\tone{25}} and Tone \ipa{\tone{23}}, tells a more nuanced
story. My studies of acoustic classification of the Cantonese tones using support vector
machines shows that what is critical for tonal identification is \textit{where} the pitch
contour is sampled. Although a consensus view has been to sample
fundamental frequency at syllable onset and offset in defining the
phonetic spaces of tones, it is clear from my computational work here
that this can be disastrous for accurate tonal identification. The
problem is that pitch rises are often realized with peak delay, where
the pitch peak occurs in the syllable \textit{after} the syllable that
the rising tone is associated to. In my Cantonese data, I found that
even the rise sometimes began only at the very end of the syllable
that it was associated to. Thus, coarse-grained sampling resolution of
the pitch contour would be sufficient for discriminating the two
confusable rises, but only if samples are taken from the syllable
following the syllable that the rises are associated to. 

The
implications of this finding for the learnability of tones is
two-fold: first, it tells us something about constraints on the hypothesis space of
possible tonal concepts. The usual way to encode parameters measured
over time is to simply treat each measured time point as another
parameter. So suppose we are only including fundamental frequency as a parameter. If we
sample fundamental frequency three times over the syllable, that's setting up a
3-dimensional space for the phonetic space of tonal contrast. If we
sample fundamental frequency 30 times over the syllable (as is done in the only tonal
category learning study \citep{Gauthier:2007}), that's setting up a
30-dimensional space for the phonetic space of tonal contrast. Suppose
that the range of allowed fundamental frequency values is something like 100-400 Hz, and
that we say that listeners can discriminate 1 Hz
increments. Combinatorics tells us that a 3-dimensional space would
allow $301^3 = 2.73 \times 10^7$ possible f0 contours; a 30-dimensional space
would allow $2.27 \times 10^{74}$. Coarse sampling resolution in
defining the input to the learner creates an exponentially smaller
hypothesis space (roughly speaking). Secondly, my finding about the
importance of \textit{where} to sample tells us  that
it is insufficient to provide input to the learner for tonal exemplars
that only includes phonetic information from the syllable that the
tone is associated to: we need more phonetic contextual
information to set up an empirically supported learning problem. In other studies of sound category learning including \citet{Gauthier:2007}, this
contextual information is not given in the input, even if the
exemplars are taken from running speech.        

\subsection{Cue weighting in learning tones}
\label{sec:cats}
 
Grounded in my study of the dimensions of tonal contrast, I
am presently working on using computational models to study how a
language learner might learn both the relevant dimensions of
tonal contrast and how to map from these dimensions to tonal
categories. The critical case study here is comparing and contrasting
input to the learner from Cantonese and White Hmong
(Hmong-Mien, Laos) using tonal production data that I have collected. I've already mentioned that \citet{Yu:2014,Yu:2011} show that the
presence of creak is used by Cantonese listeners in tonal perception,
and this implies that voice quality information beyond fundamental
frequency is useful for classifying Cantonese tones, especially for
discriminating between \ipa{\tone{21}} and \ipa{\tone{22}}. It turns
out that White Hmong has essentially the same tonal contrast as the one just
mentioned in Cantonese, but \citet{Garellek:2013} shows that listeners
do not use the presence of creak in classifying the two
tones. However, White Hmong listeners do use the presence of breathy
phonation to classify two similar falling tones. 

These empirical data give rise to the question: how does 
input to the learner determine how it weights phonetic parameters defining tonal
contrast? I am studying this by comparing the acoustic characteristic of the Cantonese
vs.~the Hmong data, especially the within- and between-class
variability in different phonetic parameters for the tones, and by observing the effect of differences in
these characteristics on clustering algorithms. 

\section{Tonal morphemes}
\label{sec:morph}

My most recent work has turned from tones that have their source in
lexical representations to tones that come from inflectional
morphology. The bulk of my efforts here thus far have centered on
Samoan, an understudied Polynesian language
\citep{Yu:2011b,Yu:2014d,Zuraw:2014a}, and I am just beginning to work
on Somali in preparation for applying for an NSF grant. I am just as
insistent as in my work on learning lexical tones that my work here be grounded in fine detail in the speech
signal.\footnote{As a case in point, \citet{Calhoun:2015} argues that
  the absolutive high boundary tone in Samoan is followed by deaccenting, and
  thus proposes that the initial phonological phrase in a Samoan
  sentence (preceding the high boundary tone) is maximally prominent
  and thus an optimal position for focused material. However, from my
  phonetic work on Samoan intonation, it is clear that what looks like
  deaccenting in her work is actually probably driven by phonetic
  effects from tonal crowding between the H- and the following
  initially stressed word.}


My fieldwork in Samoan has uncovered a high boundary tone (H-) that precedes absolutive
arguments. The distribution of this tone is identical to that
of a particle \textit{ia} which has previously been proposed to
optionally precede
absolutive arguments, but has been little studied.\footnote{I have just begun
  conversations with expert Austronesian syntactians about it (Sandy
  Chung, Diane Massam, and Masha Polinsky); they were hitherto unaware
  of Samoan \textit{ia}.}
The evidence for this tonal absolutive case morpheme has come from
phonetic and phonological analysis of intonation in a range of syntactic structures: transitive and intransitive sentences with
specific and non-specific nominals, ditransitives, nominalizations,
sentences with preverbal pronominal clitics or \textit{pro}-drop, pseudo-noun
incorporation, and extraction out of relative clauses. The source of the H- cannot be the
phonological grammar, since the presence of the H- is insensitive to
variation in prosodic length, and since there is nothing fixed about
the linear position of the absolutive in the sentence that could point
to a regularity in prosodic phrasing surrounding the absolutive. It also
can't be syntactic structure, since the H- is absent before implicit
absolutives and does not target any fixed syntactic node. It also
can't be pragmatics, since the presence of the absolutive H- is
insensitive to informational and contrastive focus. Thus, I can only
conclude that the absolutive high has its source in the
morphology. 

My claim that the Samoan absolutive
high is a tonal case marker introduces three puzzles. The first is the implication that Samoan has a single
tonal inflectional morpheme---namely, the absolutive H-, while all
other inflectional morphemes are segmental. I have addressed this puzzle
by proposing a plausible diachronic process of tonal reassociation of
a pitch accent after deletion of its host, absolutive \textit{ia}, to
a right-aligned prosodic edge immediately preceding the absolutive
argument. The second puzzle I have confronted is the asymmetry of a
prosodic boundary always preceding the absolutive argument (implicit in the
presence of the absolutive high tone), when there is no tonal evidence of a
obligatory prosodic boundary preceding non-absolutive arguments. I
conjecture that either that: (1) non-absolutive arguments do initiate prosodic
domains just like absolutive arguments, but that these domains are not
tonally marked, or (2) constraints requiring that prosodic
constituency be a faithful reflection of syntactic are relaxed in
favor of contrast preservation for case marking. Finally, I point out that the boundary
paradox introduced by the absolutive high tone---grouping the
absolutive case marker with the preceding prosodic
constituent---not only patterns after the grouping behavior of the
other non-absolutive segmental case markers, but is also cross-linguistically the norm. 

The existence of an tonal absolutive case marker in Samoan has
implications for Austronesian morphosyntax and the syntax of
ergativity, the morphosyntax-phonology interface, and the role of tone
in grammar. While previous literature on Samoan has discussed unmarked
case, the presence of optional \textit{ia} and the absolutive H-
discriminate between structures where case \textit{is} marked for
absolutive case, and structures where no overt case marking appears to
be present. For instance, the lack of an H- and the illicitness of
\textit{ia} before the (pseudo)-incorporated object in verb-initial
sentences contrasts with the presence of an H- and the licitness of
\textit{ia} before the subject in pseudo noun incorporation (PNI) sentences. This distribution supports
\citet{Massam:2001}'s analysis of PNI in Niuean which states that
PNI involves fronting of the whole VP, with the PNI object being the
internal NP inside; this NP has no case feature, but the agent DP
argument can check absolutive case (\citep[(10)]{Massam:2001}.

The existence of an absolutive H- also is reason for both optimism and
pessimism about prosody as a transparent diagnostic for morphosyntactic
structure \citep{Steedman:1991,Wagner:2010}. On the one hand,
absolutive case appears to imply the presence of an H- unconditionally
(though the phonetic realization of the H- may sometimes be difficult
to discern). On the other hand, the homophony of the
absolutive H- with other H-s from a variety of sources in Samoan means
that the presence of an H- alone cannot diagnose absolutive
case for a linguist, parser, or learner.

The origin of the absolutive H- raises questions about the
relation between processes that have been observed in tonal languages
and diachronic and synchronic tonal processes within intonational
systems. But perhaps the biggest puzzle raised by the absolutive H- is
how a tone whose source is inflectional morphology relates to prosodic
(and syntactic) constituency. \citet{Selkirk:2011} and others
have suggested that demands from phonological constraints may preclude
isomorphism between prosodic and syntactic constituency. Can demands
from morpheme realization, too? Or is Samoan prosody a reminder that
prosodic domains are demarcated by other cues in the speech signal
than the realization of tonal targets?  

\medskip

My work on tone and grammar moving forward will center on Somali,
which is spoken by a large refugee community in nearby Springfield. 
As an Institute of Social Science Research fellow for the 2014-2015
academic year, I am developing a grant for this work, to be submitted to NSF
Linguistics in January 2016. Somali is extremely unusual in the
richness of linguistic contrasts carried by tone. Tone marks gender,
e.g.\ \textit{inan} with high-low tone is ``boy'', but \textit{inan}
with low-high tone is ``girl''. Tone can also mark case, singular vs.\
plural, distinct classes of nouns, and loci of emphasis in an
utterance. While Somali tone is complex is in the sense that it marks
many different things, the relation between tone and morphology and
sentence structure is also very clear. The
extremely direct mapping between tone and morphology and syntax in
Somali makes it an ideal candidate for psycholinguistic work on the role of tone in
real-time sentence processing, and for computational work on the
learnability of inflectional tone systems. The local Somali refugee
population makes such study possible, and I am looking forward to
working with speakers to first uncover the phonetics of Somali tone
and then use that knowledge to design and carry out psycholinguistic and
computational work.     

\bibliographystyle{linquiry2}
\bibliography{4-2}


\end{document}
