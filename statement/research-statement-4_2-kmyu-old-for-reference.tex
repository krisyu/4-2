%Time-stamp: <2014-06-13 10:59:34 amoebe>
\documentclass[11pt]{article}

\usepackage{caption,subcaption,pdflscape}
\usepackage{latexsym,amsmath,amssymb,amsthm}
\usepackage{graphicx,epic,eepic,color,hyperref} 
\usepackage{epsexe}
\usepackage{natbib}
\bibliographystyle{plainnat}
\usepackage{timestamp}
\usepackage[letterpaper,top=2.5cm,bottom=2cm,left=2cm,right=2cm,head=1cm,foot=1cm,marginpar=30pt]{geometry}
\usepackage[x11names, rgb]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{lmodern}
\usepackage{times}
\usepackage[american]{babel}

\usepackage{tipa}
\let\ipa\textipa

\definecolor{neonblue}{rgb}{0.3,0.3,1}
\definecolor{irishflag}{rgb}{0.0,0.6,0.0}
\definecolor{firebrick}{rgb}{0.56,0.14,0.14}
\definecolor{navy}{rgb}{0.0,0.0,0.5}

\hypersetup{colorlinks,breaklinks,
            linkcolor=firebrick,urlcolor=neonblue,
             anchorcolor=navy,citecolor=irishflag}
\usepackage{fancyhdr}
\usepackage{lastpage}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{natbib}

\newenvironment{packed_enum}{
\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{enumerate}}

\newenvironment{packed_item}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{itemize}}

\setlength{\bibsep}{0.0pt}
\setlength{\headheight}{15.2pt}
\setlength{\headsep}{12pt}
\pagestyle{fancyplain}
\fancyhf{}
\lhead{4.2 research statement DRAFT \timestamp}
\rhead{Kristine Yu, UMass Department of Linguistics}

\rfoot{\thepage\ of \pageref{LastPage}}

\title{Research statement}

\begin{document}
\date{}
\maketitle

My research centers on tone and takes two connected paths: the study
of the phonetics of tonal contrast (\S\ref{sec:tonal-contrast}) and
the study of the distribution of tones in the sentence (\S\ref{sec:tonal-distribution}).

\section{The phonetics of tonal contrast}
\label{sec:tonal-contrast}

\subsection{Dissertation work}
\label{sec:dissertation}

My work on the phonetics of tonal contrast began in my dissertation,
where I argued against the common perspective that: 

\begin{quote}
  \textsf{Tone levels (and combinations thereof) are defined along a single
  parameter, F0; and there is no acoustic (nor as yet, articulatory)
  evidence for intersecting phonetic dimensions in F0-based tone
  systems. \\
  \hfill Clements, Michaud, and Patin (2011: 20)}
\end{quote}

My thesis work unpacked the contribution of f0 in tonal contrast in
densely specified tonal systems,
emphasizing in particular that tonal contrast unfolds in time in the
speech signal. I studied Cantonese tone perception in the face of
signal degradation decreasing the fine-grained resolution available in
the speech signal, showing that fine-grained resolution is not
necessary for good human or machine classification, although some
studies of tonal contrast assume fine-grained resolution in the f0
time course. I also studied the role of adjacent phonetic context in
Cantonese tonal perception, confirming the informativity of f0 from
the preceding syllable for tonal identification, and uncovering a role
for the following syllable as well, especially for the contrast
between the two rising tones in Cantonese. Finally, I showed that
creak occurs systematically more frequently on the lowest tone in
Cantonese and that listeners use creak in Cantonese tonal
perception--thus providing evidence for intersecting phonetic
dimensions in an f0-based tone system.

The publications from my dissertation are:

\subsubsection{Publications}
\label{sec:contrast-pubs}


\begin{packed_enum}
\item \textit{The role of creaky voice in Cantonese tone perception}
  \begin{packed_item}
    \item Authors: Kristine M. Yu and Hiu Wai Lam
    \item Journal: Journal of the Acoustical Society of America
    \item Status: 2nd revision submitted for review by associate
      editor only [2014-06-10]
  \end{packed_item}
\item \textit{Temporal resolution in Cantonese tone perception}
  \begin{packed_item}
  \item Authors: Kristine M. Yu
  \item Journal: Speech Communication
  \item Status: Final draft near completion, submit by end of June
  \end{packed_item}
\item \textit{Contextual effects in Cantonese tone perception}
  \begin{packed_item}
  \item Authors: Kristine M. Yu
  \item Journal: Journal of Phonetics
  \item Status: need to revise analyses, submit by end of summer
  \end{packed_item}
\end{packed_enum}

\subsection{Post-dissertation work}
\label{sec:post-diss}

I have continued studying the phonetics of tonal contrast since my
dissertation. One contribution I have made to this area is organizing
a workshop on the phonetics of tonal contrast June 2-3, 2014 at
University of Massachusetts. This workshop brought together the
leading scholars in this area to discuss issues of tonal contrast in a
diverse range of languages. The workshop was dominated by perspectives
from ``exotic'' tone systems, with fifteen tones, five level tones, or
contrastive alignment, and the workshop also highlighted a prominent
role for phonation in tonal contrast. 

In contrast, my current work on Bole, a two-tone Niger-Congo language takes on the
study of tonal contrast from a language with only a H and a L
tone. This work uses Bole as a case study in the typology of tonal
contrast to show that voice quality can play a role in the acoustics
of tonal contrast even in this simplest of tonal systems, and that
there is considerable variability in the alignment of tonal targets in
Bole, Igbo, and Yoruba.

A future project addresses an issue that came up repeatedly during the
workshop: our lack of understanding of the perception of creak and how
this interacts with pitch perception. Together with John Kingston, I
am beginning a study this summer investigating the perceptual
integration of creak and fundamental frequency in non-tonal and tonal languages.


\section{The distribution of tones in the sentence}
\label{sec:tonal-distribution}

My second stream of research centers on non-lexical tones in the
sentence and has been organized among the following questions:

\begin{exe}
  \ex \textit{What are the sources of non-lexical tones in the sentence?}
  \ex \textit{How do these different sources interact in conditioning speakers' prosodic choices?}
  \ex \textit{Given these different sources, how may these tones be used in sentence processing?}
\end{exe}

\subsection{Sources of non-lexical tones and their interactions}
\label{sec:sources}

A classic divide between sources of intonational tones is the divide
between para-language and language. Recently, attention in the broader
community of prosody research has shifted toward paralinguistic
aspects of prosody, i.e.\ affective/emotional and social sources, quite divorced
from and even denying the role of linguistic grammar. My joint work compares
intonational phonology in non-infant directed and infant-directed
speech in Bengali and English to respond to this perspective. We argue that emotional/attentional
motivation for prosodic choices by parents in infant-directed speech
are constrained by language-specific intonational grammar. 
Changes in the distribution of tones between non-infant directed and
infant-directed speech are indeed consistent with increasing high pitch
targets and the number of turning points within tones---acoustic
changes known to engage and maintain infant attention. However, these
prosodic choices are constrained by intonational grammar: for
instance, in Bengali, an increase in complex boundary tones such as
HLH\% in infant-directed speech introduces more pitch modulation into
the signal. But this modification is not available in the English
intonational grammar and is not observed; rather, English shows an
increase in the bitonal L+H* pitch accent. Moreover, changes in parents' prosodic
choices in infant directed speech also reflect greater highlighting of
information structure---not just any tone contributing to
lhigher pitch and more pitch modulation occurs shows increased use in
infant-directed speech. 

\begin{tabular}[h]{c c}
  \includegraphics[width=0.5\textwidth]{figs/bengali2}
  & \includegraphics[width=0.35\textwidth]{figs/tobi2.png}\\
Bengali tonotactic grammar  & English tonotactic grammar\\
\end{tabular}

\hfill\break
\hfill\break
Among linguistic sources of intonational tones, a classic division is
between: (a) tonal morphemes, e.g.\ functional heads, and (b) markers
of prosodic grammatical structure. My work in Samoan focuses on
adjudicating between these two possibilities as the source of a high
tone. This high tone occurs at the right edge of the constituent
preceding an absolutive argument. I use systematic manipulation of
word order, case-marking, argument structure and prosodic length to
argue that this tone is a tonal morpheme that marks absolutive
case. For prosodic markedness reasons, this tonal morpheme must occur
at the right edge of a prosodic constituent. However, while the high tone is
realized as an edge tone, the source of the tone is in the morphology,
not in the prosodic grammar.

\subsubsection{Publications}
\label{sec:sources-pubs}

My publications on work exploring the sources of intonational tones are:

\begin{packed_enum}
\item \textit{Intonational phonology in Bengali and English infant-directed speech}
  \begin{packed_item}
    \item Authors: Kristine M. Yu, Sameer ud Dowla Khan and Megha Sundara
    \item Journal: Laboratory Phonology
    \item Status: Manuscript in preparation, to be submitted by end of August
  \end{packed_item}
\item \textit{Tonal case marking in Samoan}
  \begin{packed_item}
    \item Authors: Kristine M. Yu
    \item Journal: Language
    \item Status: Manuscript in preparation, to be submitted by end of July
  \end{packed_item}
\end{packed_enum}


\subsection{Future work: prosodic parsing}
\label{sec:future-parsing}

Given the rich inventory of sources of tones in the sentence, how
might these tones be used in recognizing fluent speech? My future work
in my second stream of research over the next few years focuses on the following hypothesis:

\begin{exe}
  \ex Hypothesis: There is a rich hierarchical prosodic representation
  that comes from phonological grammar that is incrementally parsed in
  sentence processing, and listeners use this to constrain hypotheses
  about the upcoming prosodic and other linguistic structure.
\end{exe}

While there is an ever-larger body of work on prosody in sentence
processing, this work focuses squarely on how listeners use prosodic
information in recognizing syntactic and semantic structure. I am
interested in particular in studying the parsing of phonological
structure. There are two key components of my hypothesis:

\paragraph{Hierarchical prosodic representation}

While there is ample evidence for the use of various prosodic cues in
sentence processing, there is little evidence for the use of
hierarchical prosodic grammar. I will build on knowledge from
computational models of syntactic parsing to carefully define different
current theories of prosodic grammar and their implications for
parsing of prosodic structure. I will also undertake experiments to
ascertain whether listeners use prosodic grammar in sentence
processing, e.g.\ if listeners are sensitive to
prosodically-conditioned allophony and stress retraction. To help
focus on parsing of prosodic structure, I will also study accentual
phrase languages such as Hindi, in which the distribution of prosodic tones is much
more regular and automatic due to prosodic grammar than in English.

\paragraph{Incremental parsing and hypotheses about upcoming prosodic structure}

In syntactic parsing, language users can use information from
non-local dependencies to revise hypotheses about upcoming prosodic
structure. In prosodic parsing, is there any kind of analogue? In
Bengali, for instance, accentual phrases are marked with a L* Ha
rise. Sequences of these rises occur within larger prosodic
phrases. In special circumstances, accentual phrases may also be
marked with a H* La fall, but speakers do not switch between falls and
rises in a sequence of accentual phrases. Thus, if speakers hear an
initial L* Ha rise, they may expect a following L* Ha rise. 

Are there ``garden paths'' in prosodic parsing in considering
alternative parses? Some sources of prosodic ambiguity come from
ambiguity between the level in the prosodic hierarchy, e.g.\ confusion
between a lower level and a higher phrase edge:

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\textwidth]{figs/beckman1996-parses.png}
  \caption{Beckman 1996: alternative prosodic parses}
  \label{fig:alternative-parses}
\end{figure}

Another source of prosodic ambiguity comes from ambiguity between edge-marking
tones and head-marking tones. In Turkish, for instance, a H* pitch
accent occurs on stressed syllables. However, a H- boundary tone also
occurs at the right edge of syntactic constituents such as NPs and
PPs. Thus, if there is word-final stress, there may be ambiguity
between whether the source of an H tone is syntactic, prosodic, or both.

\end{document}
 
\leftline{{\Large\bf Prosodic parsing}}
\hfill\break

\section{What is the evidence for hierarchical (phrasal) phonological
  structure?}

\begin{exe}
  \ex This is different than saying there are domains, which could
  be achieved by introducing a boundary symbol into the alphabet. You
  could get this with a regular grammar.
  \begin{xlist}
    \item Boundary symbols aren't enough because they don't capture
      the unarbitrariness of the demarcation of domains, e.g.\ Samoan
      bisyllabic vs.\ monosyllabic affixes.
    \item Systematic tonal marking of recursion, e.g.\ Irish.
    \item Systematic correlation of various measures of ``strength''
      via articulatory/acoustic measures, e.g.\ domain-initial strengthening.
  \end{xlist}
  \ex What's the evidence for prosodic constituency?
  \begin{xlist}
    \item Click tests? Review prosodic vs. syntactic constituency
      tests.
    \item Anaphoric relations? Movement?
  \end{xlist}

\end{exe}



\section{Incremental prosodic parsing}

\begin{exe}
  \ex Anticipatory H tone raising?
  \ex Spreading?
  \ex Prosodic parsing principles?
  \ex Accentual phrase type languages? Indian English? 
  \ex Prediction of upcoming prosodic structure
  \begin{xlist}
    \item Cutler 1976 and follow-up studies
    \item Guess end of sentence? 
  \end{xlist}
\end{exe}

\section{The richness of prosodic structure in incremental parsing}

\begin{exe}
  \ex Lots of work on edges, but do we wait until we get to edges? Or
  are we parsing prosodic structure all along? (Kjelgaard and Speer)
  \ex A lot of so called domain edge-conditioned phenomena could also
  be recast as domain internal phenomena, e.g.\ domain-edge fortition
  as domain-internal lenition
  \ex The propellor?
  \ex Abstract phonological phenomena conditioned on prosodic domains?
  \begin{xlist}
    \item Rhythm rule?
    \item Early accent?
  \end{xlist}
\end{exe}



\section{Prosodic ambiguity}

\begin{exe}
\ex Ambiguity of phrasing
\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\textwidth]{beckman1996-parses.png}
  \caption{Beckman 1996}
  \label{fig:alternative-parses}
\end{figure}

\ex Ambiguity of edge tone vs.\ accent
\begin{xlist}
  \item Turkish H?
\end{xlist}
\end{exe}



\section{What is the evidence for hierarchical syntactic structure?}


\end{document}