%Time-stamp: <2015-01-26 05:10:49 amoebe>
\documentclass[12pt]{article}

\usepackage{caption,subcaption,pdflscape}
\usepackage{latexsym,amsmath,amssymb,amsthm}
\usepackage{graphicx,epic,eepic,color,hyperref} 
\usepackage{epsexe}
\usepackage{natbib}
\bibliographystyle{plainnat}
\usepackage{timestamp}
\usepackage[letterpaper,top=2.5cm,bottom=2cm,left=2cm,right=2cm,head=1cm,foot=1cm,marginpar=30pt]{geometry}
\usepackage[x11names, rgb]{xcolor}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%\usepackage{lmodern}
\usepackage{times}
\usepackage[american]{babel}

\usepackage{tipa}
\let\ipa\textipa

\definecolor{neonblue}{rgb}{0.3,0.3,1}
\definecolor{irishflag}{rgb}{0.0,0.6,0.0}
\definecolor{firebrick}{rgb}{0.56,0.14,0.14}
\definecolor{navy}{rgb}{0.0,0.0,0.5}

\hypersetup{colorlinks,breaklinks,
            linkcolor=firebrick,urlcolor=neonblue,
             anchorcolor=navy,citecolor=irishflag}
\usepackage{fancyhdr}
\usepackage{lastpage}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}
\usepackage{natbib}

\newenvironment{packed_enum}{
\begin{enumerate}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{enumerate}}

\newenvironment{packed_item}{
\begin{itemize}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{itemize}}

\setlength{\bibsep}{0.0pt}
\setlength{\headheight}{15.2pt}
\setlength{\headsep}{12pt}
\pagestyle{fancyplain}
\fancyhf{}
\lhead{\textbf{4.2 teaching statement}}
\rhead{Kristine M. Yu, UMass Department of Linguistics}

\rfoot{\thepage\ of \pageref{LastPage}}

\title{Teaching statement}

\begin{document}
\date{}
%\maketitle

My current emphasis in teaching is to guide students into discovering
how to think rather than to tell them how others have thought. I begin
this discovery process by having students confront raw empirical
data before discussing how we might analyze it. For instance, in the
first day of my class this semester on phonological theory (606), we
elicited words from a fellow student who is a native Vietnamese speaker to
discover the tones in the language. Students discovered the
challenges in trying to transcribe and represent tones. I then used
their experience in the next class meeting to motivate, discuss, and challenge how linguists have represented
tones. In continuing the discovery process of how to think, I draw on
the diversity of backgrounds of the students to connect
what we are learning to their particular areas of interest. In my phonology
class this semester, one way I am doing this is by taking advantage of
the rich
diversity in the language backgrounds of my students and having each one
serve in turn as a language consultant to illustrate the particular
phonological phenomenon at hand.           

I have gradually developed my emphasis on guiding students to discover how
to think from my teaching experiences at UMass so far.  In my first two years at UMass, I taught one introductory core
graduate-level class (606, Phonological Theory, Spring 2013), one
advanced undergraduate/graduate-level course (592B, Speech Processing,
Spring 2014), and four graduate seminars:

\begin{packed_item}
\item 730 (Proseminar--Phonological Theory: Prosody), Fall 2012
\item 748 (Structure of a non Indo-European language), Spring 2013
\item 716 (Topics in Phonetics: Categories), Fall 2013
\item 751 (Topics in Phonology: Prosodic Parsing), Spring 2014
\end{packed_item}

From my experiences with teaching these classes, I have identified three
challenges that I have been working (and will continue working) to
meet: (1) balancing teacher and student co-ownership of the classroom, (2) cultivating student ownership
of learning, and (3) bringing along students of extremely varying
backgrounds. My emphasis on helping students discover how to think
comes as a guiding principle to confront these challenges that I describe below.

\section{Balancing teacher and student co-ownership of the classroom}

How does one negotiate the teacher and the students' co-ownership of a
course---of the course material, of the workload, of in-class
discussions, of the learning process and experience? After a rough
patch in my first year working to figure this out, I have developed
strategies to effectively assert my co-ownership of the course while
still respecting the views of the students. 

One strategy I've
implemented is setting a rigid selection of topics for the
first weeks of the course to give the students a common base of
knowledge to draw on, no matter what they come in with. However, I leave the later weeks of the course
open to significant adjustment, depending on student interests that
emerge over the semester. The time I spend bringing all the students to
the same point in the first part of the course allows the students to
gradually take on greater and greater responsibility for the direction
of the course as it progresses.

Another strategy I've adopted is being selective about when and how I
ask students for feedback in the course of the semester. I discovered
in my first year of teaching that being open to constant feedback from
students can spiral into a unproductive situation of continuous negotiation between
me and them throughout the semester about every aspect of the
course. Therefore, I have limited soliciting of student feedback
during the semester to midway through the course via an online survey, and I don't ask for
feedback on things I am not willing to change. This spring I will formally collect mid-term
feedback from students through the Center for Teaching
and Faculty Development Midterm Assessment Program.

\section{Cultivating student ownership of learning}

In my second year of teaching, I became especially aware of another challenge: the challenge of cultivating student ownership of
learning. In the 500-level and advanced graduate seminars I taught in Fall 2013 and
Spring 2014, the class format was lecture peppered with
discussion and I was often frustrated by the lack of student participation. Nowhere did it become clearer to me that my delivering
content is not equivalent to the students learning it than near the end
of the semester in my 500-level class. When I asked
what I thought was a very basic check-in question about the topic I had been
covering for the last month, I was shocked that no one could (or was willing) to answer
it. Since I had stopped assigning homework to let the
students focus on their final projects, the students hadn't been
taking the time to digest the class material. However, when the
students had to use that course material for their final
projects, they did grapple with it and applied the concepts and tools
learned in creative ways to the research problems they had chosen to
work on---I was very proud of their work!

The contrast between the results of my lecturing and the results of
having students actively use the concepts learned in tackling hairy
problems was striking. I am now excited to try elements of a
team-based learning approach to achieve the intensity of student
engagement I want. In Spring 2015, my colleague John Kingston will be
trying this approach in his 414 undergraduate phonetics class for the
first time, and I will also be involved in the course design, as I
will be taking over teaching that course as of the next academic year. I'm
looking forward to the learning experience!


\section{Bringing along students of varying backgrounds}

I think elements of a team-based learning approach can also help with
the third key challenge I have identified in my teaching thus far:
dealing with and even leveraging the diversity of backgrounds of my students. 

In the core graduate phonology course I've
taught, this manifests as a bifurcation between well-prepared students who are
already on track to specializing in phonology and thus inherently
interested in the course material, and those who are
already on track to specialize in some other sub-field and thus
sometimes not so invested in the course material. What is the best way
to serve both crowds? 

One approach I have introduced to help leverage the diverse
backgrounds of the students is to institute a series of joint lectures
between the introductory syntax and phonology classes towards the end
of the year. I piloted this in Spring 2013, and the course instructors
last year repeated this, and I plan to do it again this year. This
provides a kind of capstone experience that helps the students (and
instructors) integrate phonological and syntactic ways of thinking. 

Another approach I am trying comes from a shift in thinking that my colleagues and I
have discussed: moving away from worrying about checking off boxes
in an overblown list of topics, and instead, concentrating on teaching the students how to
think. Inspired by this reorientation, I am overhauling my
approach to teaching the graduate phonology course in Spring 2015. I plan to be quite
parsimonious about the volume of material covered, in favor of guiding
the students in deep thinking about a few core phonological
problems. Rather than just telling the students about the material, I will
help them discover it for themselves in a team-based learning approach. For instance, I am developing a
unit on phonological features centered on the problem of computing the similarity between
speech sounds. The students will tinker with a couple different
computational implemented models of phonological similarity using
different assumptions about what the set of features might be and how
they might index similarity. 

In the 500-level course I designed on speech processing, some students
come in not knowing what an integral is; others come in having taken
graduate courses in machine learning; all of them come with varying
levels of experience with speech. Last Spring (2013) when I first
taught this course, I was delighted to find a book introducing Fourier
analysis from the ground up (\textit{Who is Fourier?}), which
explained all the basic calculus needed in geometric and visual
terms. I also provided notes from a graduate level engineering course
on the Fourier Transform for the more mathematically mature
students. Still, it was incredibly difficult to strike a balance
between slogging through the mathematical and computational background
needed for students to get a deep understanding of speech processing,
and just providing hand-wavy explanations to give them an intuition
for what was going on. And I wasn't satisfied with the balance of
introducing linguists to signal processing and computation whilst also
introducing speech to the computer scientists.   

The next time I teach this class in Spring 2016, I am also excited to
try incorporating elements of
team-based learning. The computer science department has started
advertising my course, so I'm hoping to attract a larger crowd of
computer scientists who I can then partner up with linguists to
exchange knowledge and approaches. Regardless of the crowd I end up
with, I plan to incorporate much more group work and individual and
group assessments and to reduce my lecturing to mini-lectures. 

I look forward to experimenting with team-based learning in this course and others to come!


\end{document}
