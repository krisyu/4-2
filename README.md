# **Kristine M. Yu 4.2 portfolio, 2014-2015**
**Assistant Professor, Department of Linguistics**

**University of Massachusetts Amherst**

# *Updates:*
+ [2015-01-26 18:09:03] Updated statements and CV for release to
  faculty; updated Samoan paper

# CV: [[pdf](https://bytebucket.org/krisyu/4-2/raw/a361aebfc6591fa1fd5246fbdaf24e3dce1cfd8b/cv/kmyu-cv.pdf)]

# Personal statements 
1. ## Research statement: [[pdf](https://bytebucket.org/krisyu/4-2/raw/d6936dce8b05529a61d82bac98e6556c10f3f527/statement/research-statement-4_2-kmyu.pdf)]
2. ## Teaching statement [[pdf](https://bytebucket.org/krisyu/4-2/raw/eaffd5b4772cbb9408adb7970e1d7009b074ff30/statement/teaching-statement-4_2-kmyu.pdf)]
    + CTFD observation report, Fall 2013, 751 Topics in Phonetics
  [[letter](https://bytebucket.org/krisyu/4-2/raw/ac665ec8c0e94288425614d786323ddff2c934b1/teaching/Yu_Letter.pdf), [report](https://bytebucket.org/krisyu/4-2/raw/ff00b5522e00fdd11309b3ea58c99b19c165c144/teaching/Yu_Observation_Results.pdf)]
3. ## Service statement [[pdf](https://bytebucket.org/krisyu/4-2/raw/eaffd5b4772cbb9408adb7970e1d7009b074ff30/statement/service-statement-4_2-kmyu.pdf)]


---------------------------------------------

---------------------------------------------

# **List of linguistics papers**

Papers are available in the [papers](https://bitbucket.org/krisyu/4-2/src/99f6bf8e13e85974ffa38e43232ed3fa22ae6b2f/papers/?at=master) directory and are also linked below:

+ [in prep](#markdown-header-planned)
+ [submitted, accepted, published](#markdown-header-out)


## *In prep*

1. **Tonal marking of absolutive case in Samoan**

    + Journal: Language
	+ Status: Submitted [pdf](https://bytebucket.org/krisyu/4-2/raw/acfcf058c24c8bb5a9dbb8293bb6c7c4cda2c7e4/papers/smo-lg.pdf)
	+ Authors: Kristine M. Yu
	+ Notes: Recently presented at UCSC Phlunch (October 17, 2014), 
      [McGill Exploring the Interfaces 3](http://eti3mcgill.wix.com/eti3)
      (May 10, 2014)
	+ Outdated versions:
      [NELS 2009/2011 paper](https://bytebucket.org/krisyu/4-2/raw/7854159f86dfc90cc85eda5be42470bbbf256405/papers/yu2011-nels39-samoan.pdf?token=01d64d18214ae00e4acb6f41c43f70507413b504),
      [ETI 3 slides](https://www.dropbox.com/sh/i4gg41pit14iypd/AAChaFFOBAvqIeigUPAkDWowa/Yu_eti3-samoan.pdf)  
    # #


  
2. **Temporal resolution in Cantonese tonal perception**

    + Journal: Journal of Phonetics
	+ Status: About to be submitted---to be submitted by November 14, 2014
	+ Authors: Kristine M. Yu
	+ Notes: (Heavily revised) dissertation work
	+ Read current draft
      [here](https://bytebucket.org/krisyu/4-2/raw/50d5c004cb808f5df0e746ac909b752d6780240b/papers/res-jphon.pdf?token=4f8a84bc07508877db202bb880beb24bc6b4cc77)  
    # #


3. **Intonational phonology in Bengali and English infant-directed
   speech**
   
    + Journal: Laboratory Phonology
    + Status: In preparation, submission by end of Fall semester
    + Authors: Kristine M. Yu, Sameer ud Dowla Khan and Megha Sundara
    + Notes: Recently presented at [Speech Prosody 7](http://www.speechprosody2014.org/) (May 23, 2014)
    + Outdated versions:
      [SP 7 proceedings](https://bytebucket.org/krisyu/4-2/raw/0c923efb3ea062fa7c9efce91cbd61b4e36e26b7/papers/sp7_submission_182.pdf?token=dc552a4d953df010579ff8d701d1046519478418)  
    # #


4. **Contextual effects in Cantonese tonal perception**

    + Journal: Journal of Phonetics
	+ Status: Need to revise analysis, will submit by end of semester
	+ Authors: Kristine M. Yu
	+ Notes: Dissertation work
    # #


-------

## *Out*

1. **The role of creaky voice in Cantonese tone perception**
   [[web](http://scitation.aip.org/content/asa/journal/jasa/136/3/10.1121/1.4887462),
   [pdf](https://bytebucket.org/krisyu/4-2/raw/50d5c004cb808f5df0e746ac909b752d6780240b/papers/yulam2014-jasa-cantcr.pdf?token=a9ff8310a5b6bee422bb1ba4866fb3f09d1f2716),
   [supplementary material](http://www.krisyu.org/blog/posts/2014/06/supp-material-cantonese-creak-perception/)]

    + Journal: Journal of the Acoustical Society of America, 136(3), 1320-1333
	+ Status: Published 2014
	+ Authors: Kristine M. Yu and Hiu Wai Lam
	+ Notes: (Heavily revised) dissertation work; second author was my undergraduate RA who
      wrote her honors thesis under my/Pat Keating's co-supervision
	+ Outdated versions:
      [ICPhS 2011 proceedings](https://bytebucket.org/krisyu/4-2/raw/7854159f86dfc90cc85eda5be42470bbbf256405/papers/yu-lam2011-icphs-cantonese-creak.pdf?token=060ae4536a8b63cd62017e0db9b43491af54fcbf)  
    # #


    
2. **The word-level prosody of Samoan**
   [[web](http://dx.doi.org/10.1017/S095267571400013X),
   [pdf](https://bytebucket.org/krisyu/4-2/raw/50d5c004cb808f5df0e746ac909b752d6780240b/papers/zurawyuorfitelli2014.pdf?token=c54abba0c0f1b9265a495f49461f13633118c92b)]
   
    + Journal: Phonology, 31(2), 271--327
	+ Status: Published 2014
	+ Authors: Kie Zuraw, Kristine M. Yu and Robyn Orfitelli
	+ Notes: I did not contribute equally with the first author.  
    # #


3. **The experimental state of mind in elicitation: illustrations from
   tonal fieldwork**
   [[web](http://scholarspace.manoa.hawaii.edu/bitstream/handle/10125/24623/Yu.pdf?sequence=1),
   [pdf](https://bytebucket.org/krisyu/4-2/raw/f25c58a04433de63b7940ee83bcecf1204f253e0/papers/yu2014-kiy.pdf),
   [supplementary material](http://www.krisyu.org/blog/posts/2013/06/ldc-kiy-overview/)]
   
    + Journal: Language Documentation & Conservation
	+ Status: Published 2014
	+ Authors: Kristine M. Yu
	+ Notes: Part of a (refereed) special collection on how to study a tone
      language edited by Steven Bird and Larry M. Hyman. Includes an
      extensive set of tutorials as supplementary materials.
    # #


4. **Taiwan Mandarin quantifiers**
   [[pdf](https://bytebucket.org/krisyu/4-2/raw/0c923efb3ea062fa7c9efce91cbd61b4e36e26b7/papers/kuo-yu-taiwan-mandarin-quantifiers.pdf?token=99e6d0ae7257178ccc5b5ed6cabbc618c15edac6)]
   
    + Book: [Handbook of Quantifiers in Natural Language](https://www.springer.com/education+%26+language/linguistics/book/978-94-007-2680-2), eds. Edward Keenan and Denis Paperno     
    + Status: Published 2011
    + Authors: Grace C.H. Kuo and Kristine M. Yu
    + Notes: Equal contribution by authors
	





