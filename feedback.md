# Notes on feedback
<!-- Time-stamp: <2014-10-05 21:11:25 amoebe> -->

## Meeting with JJM/JP summer
1. Feedback on publication record
    1. Get dissertation stuff out now
	2. Mine things you've already done
2. Feedback on research program, grant
    1. Don't expect you to just go off in completely different
       direction
	2. Important part is for you to be productive
	3. Build on what you already have a track record in

## Meeting with JK on publications 2014-10-01
1. Feedback on progress on journal articles out and in prep
     - Good, what's expected at this stage, to mine graduate work and get work out from that
2. Suggestions for further progress		 
    1. Get a grant
	2. Establish elevator speech--cohesiveness to research program

